#!/usr/bin/python3
# -*- encoding: utf-8 -*-

"""
compile - Sequentially compile the whole DesQ project

###================== Program Info ==================###
    Program Name : compile
    Version : 1.0.0
    Platform : Linux/Unix
    Requriements :
        Must :
            modules os, sys
    Python Version : Python 3.4 or higher
    Author : Marcus Britanicus
    Email : marcusbritanicus@gmail.com
    License : Public Domain
###==================================================###
"""

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

    #
    # This script is in public domain; do whatever you want to do with it.
    #

    #
    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    #

### =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #

import os, sys

Project = [
    # Libraries
    "libdesq",
    "libdesqui",

    # Session
    "Session",

    # Shell UI
    "Shell",

    # Theme
    'Theme',

    # DesQ Utils
    ## "DesQUtils/Clipboard",
    ## "DesQUtils/Clock",
    "DesQUtils/Disks",
    ## "DesQUtils/Dock",
    "DesQUtils/Keyring",
    "DesQUtils/Lock",
    ## "DesQUtils/OSK",
    "DesQUtils/Notifier",
    "DesQUtils/Panel",
    "DesQUtils/PolkitExec",
    "DesQUtils/PowerManager",
    "DesQUtils/Runner",
    "DesQUtils/SNI",
    "DesQUtils/Splash",
    ## "DesQUtils/SshAskPass",
    "DesQUtils/Volume",

    # DesQ Apps
    'DesQApps/DesQArchiver',
    'DesQApps/DesQDropDown',
    'DesQApps/DesQDocs',
    'DesQApps/DesQTerm',
    ## 'DesQApps/DesQFiles',
    ## 'DesQApps/DesQEye',
    ## 'DesQApps/DesQStats'
    ## 'DesQApps/DesQText'
]

def uninstall() :

    for proj in Project:
        if not os.path.exists( f"{proj}/.build/meson-logs/install-log.txt" ):
            print( f"{proj} not installed. Nothing to uninstall." )
            continue

        os.system( f"cd {proj}; sudo ninja -C .build uninstall" )


if __name__ == '__main__' :

    if "--uninstall" in sys.argv:
        uninstall()
        exit( 0 )

    import argparse

    parser = argparse.ArgumentParser( description = 'Compile and install the DesQ Project', add_help = False )
    parser.add_argument( "-h", '--help', help = 'Print this help and exit', action = "help" )
    parser.add_argument(
        "-p", '--prefix',
        help = "Install everything to <prefix>. Defaults to '/usr'. Provide absolute path.",
        dest = "prefix",
        default = "/usr"
    )

    args = parser.parse_args()

    meson = f"meson .build --prefix={args.prefix} --buildtype=release"

    for proj in Project:
        ## If it's a project name
        if not os.path.exists( os.path.basename( proj ) ):
            ret = os.system( f"git clone https://gitlab.com/DesQ/{proj}" )
            if ( ret ) :
                if ( proj in [ "libdesq", "libdesqui" ] ):
                    print( f"Failed to clone essential repo: https://gitlab.com/DesQ/{proj}. Exiting..." )
                    exit( 1 )

                else:
                    print( f"Failed to clone the repo: https://gitlab.com/DesQ/{proj}. Continuing..." )
                    continue

            os.chdir( proj.split( "/" )[ -1 ] )

        else:
            os.chdir( proj.split( "/" )[ -1 ] )
            ret = os.system( "git pull" )
            if ( ret ) :
                if ( proj in [ "libdesq", "libdesqui" ] ):
                    print( f"Failed to pull the latest changes of essential repo: https://gitlab.com/DesQ/{proj}. Exiting..." )
                    exit( 1 )

                else:
                    print( f"Failed to pull the latest changes of the repo: https://gitlab.com/DesQ/{proj}. Continuing..." )
                    continue

        ret = os.system( cmake )
        if ret:
            print( f"Meson failed to create build.ninja file. " )
            if ( proj in [ "libdesq", "libdesqui" ] ):
                print( f"Exiting..." )
                exit( 1 )

            else:
                print( f"Continuing..." )
                continue

        else:
            ret = os.system( "ninja -C .build -k 0 j$(nproc)" )

        if ret :
            print( "Some error was encountered during compilation." )
            print( f"Please create an issue https://gitlab.com/DesQ/{proj}/-/issues/" )
            print( f"You may also contact the devs at https://matrix.to/#/#desq:matrix.org" )

        else:
            ret = os.system( "sudo make install" )

        if ret :
            print( "Some error was encountered during installation." )
            print( f"Please create an issue https://gitlab.com/DesQ/{proj}/-/issues/" )
            print( f"You may also contact the devs at https://matrix.to/#/#desq:matrix.org" )
            sys.exit( 1 )

        os.chdir( "../" )
